# Patient Health Records: App Modernization with Red Hat OpenShift

# Contents

- [Patient Health Records: App Modernization with Red Hat OpenShift](#patient-health-records-app-modernization-with-red-hat-openshift)
- [Contents](#contents)
- [Introduction to Health-App](#introduction-to-health-app)
  - [Example Health Background Story](#example-health-background-story)
  - [Project aims](#project-aims)

---

# Introduction to Health-App

![health-App](./images/health-app-espanol.png)

## Health App Pipeline

![health-app-pipe](./images/pipeline-health-app.png)

This project is a patient records user interface for a conceptual health records system. The UI is programmed with open standards JavaScript and modern, universal CSS, and HTML5 Canvas for layout.

The UI is served by a simple Node.JS Express server, and the overall project goals are:

- To use the project to show a step-by-step guide of deploying the app on OpenShift Source to Image (S2I). Also, we have modified the source code to integrate a Jenkins pipeline to build this image.
- To illustrate the versatility of Kubernetes based microservices for modernizing traditional applications - for instance mainframe based applications, or classic Java app server applications.
- To experiment and explore open standards front end technologies for rendering custom charts, and for responsive design.

This project has been modified to integrate automatically with a JAVA API.


---

## Example Health Background Story

Example Health is a pretend, conceptual healthcare/insurance type company. It is imagined to have been around a long time, and has 100s of thousands of patient records in an SQL database connected to an either a mainframe, or a monolithic Java backend.

The business rules for the system is written in COBOL or Java. It has some entitlement rules, prescription rules, coverage rules coded in there.

Example's health records look very similar to the health records of most insurance companies.

Here's a view a client might see when they log in:

![screenshot](./design/mockup.png)

Example Health business leaders have recently started understanding how machine learning using some patient records, might surface interesting insights that would benefit patients. There is lots of talk about this among some big data companies.

https://ai.googleblog.com/2018/05/deep-learning-for-electronic-health.html

https://blog.adafruit.com/2018/04/16/machine-learning-helps-to-grok-blood-test-results/

[ concept screenshot to come ]

Example has also heard a lot about cloud computing. There is a lot of traditional code in the mainframe and in classic Java app servers. It works well for now ... but some software architects think it may be complimentary to explore some machine learning, and to accelerate development of new user interfaces in the cloud (either public or private).

---

## Project aims

In this repo there is a patient user interface. It is written using plain HTML, CSS and JavaScript served from a Node.js microservice. The code runs by default with test/demo data, that doesn't rely on a more sophisticated server. The following installation steps can help you easily deploy this using OpenShift S2I (source to image).

---

